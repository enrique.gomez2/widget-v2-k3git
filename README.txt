CSCI 40-F
Atienza, Miguel Luis Antonio, A, 210523; Benito, Matthew Josh Benedict, R., 210857; Garsin, Mariam Yasmin, B, 206034; Gomez, Enrnique Jose Stefan, P, 212804; Que, Nate Brevin, A, 214754;
Final Project: Widget v2
App Assignments:
    Dashboard - Stefan
    Announcement Board - Josh
    Forum - Migs
    Assignments - Brevin
    Calendar - Yam
DATE OF SUBMISSION: May 15, 2023
The project was done completely by k3git without help from people outside of the group.
REFERENCES USED:

Members Signatures:
[sgd] Atienza, Miguel Luis Antonio A., 
[sgd] Benito, Matthew Josh Benedict R.,
[sgd] Garsin, Mariam Yasmin, B.,
[sgd] Gomez, Enrique Jose Stefan P.,
[sgd] Que, Nate Brevin A.,


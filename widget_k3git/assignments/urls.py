from django.contrib import admin
from django.urls import path

from .views import index, AssignmentDetailView, AssignmentCreateView, AssignmentUpdateView

urlpatterns = [
    path('', index, name='index'),
    path('<int:pk>/details', AssignmentDetailView.as_view(), name='assignment-details'),
    path('add/', AssignmentCreateView.as_view(), name='add-assignment'),
    path('<int:pk>/edit', AssignmentUpdateView.as_view(), name='edit-assignment'),
]

app_name = "assignments"
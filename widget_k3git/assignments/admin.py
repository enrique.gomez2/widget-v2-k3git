from django.contrib import admin

from .models import Course, Assignment


class CourseAdmin(admin.ModelAdmin):
	model = Course


class AssignmentAdmin(admin.ModelAdmin):
	model = Assignment


admin.site.register(Course, CourseAdmin)
admin.site.register(Assignment, AssignmentAdmin)
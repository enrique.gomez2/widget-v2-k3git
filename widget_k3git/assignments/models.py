from django.db import models
from django.urls import reverse


class Course(models.Model):
	code = models.CharField(max_length=10)
	title = models.CharField(max_length=100)
	section = models.CharField(max_length=3)

	def __str__(self):
		return '{} {}-{}'.format(self.code, self.title, self.section)


class Assignment(models.Model):
	name = models.CharField(max_length=100)
	description = models.TextField()
	course = models.ForeignKey(Course, on_delete=models.CASCADE)
	perfect_score = models.IntegerField()

	def __str__(self):
		return """Assignment Name: {}<br>
				Description: {}<br>
				Perfect Score: {}<br>
				Passing Score: {}<br>
				Course/Section: {}<br>""".format(self.name, self.description, self.perfect_score,
											self.passing_score, self.course)

	def get_absolute_url(self):
		return reverse('assignments:assignment-details', kwargs={'pk':self.pk})

	@property
	def passing_score(self):
		return round(self.perfect_score*6/10)
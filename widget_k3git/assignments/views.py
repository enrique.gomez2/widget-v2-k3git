from django.shortcuts import render
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView

from .models import Assignment


def index(request):
	return render(request, 'assignments/assignments.html', {'assignments': Assignment.objects.all()})


class AssignmentDetailView(DetailView):
	model = Assignment

	def get(self, request, pk):
		return render(request, 'assignments/assignment-details.html', {'assignment': self.model.objects.get(pk=pk)})


class AssignmentCreateView(CreateView):
	model = Assignment
	fields = '__all__'
	template_name = 'assignments/assignment-add.html'


class AssignmentUpdateView(UpdateView):
	model = Assignment
	fields = '__all__'
	template_name = 'assignments/assignment-edit.html'
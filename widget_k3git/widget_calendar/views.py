from django.shortcuts import render
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView

from.models import Event


def index(request):
    return render(request, 'widget_calendar/calendar.html', {'events': Event.objects.all()})


class EventDetailView(DetailView):
	model = Event

	def get(self, request, pk):
		return render(request, 'widget_calendar/event-details.html', {'event': self.model.objects.get(pk=pk)})
	

class EventCreateView(CreateView):
	model = Event
	fields = '__all__'
	template_name = 'widget_calendar/event-add.html'
	

class EventUpdateView(UpdateView):
	model = Event
	fields = '__all__'
	template_name = 'widget_calendar/event-edit.html'
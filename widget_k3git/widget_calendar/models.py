from django.db import models
from django.utils import timezone

from django.urls import reverse


class Location(models.Model):
    ONSITE = 'onsite'
    ONLINE = 'online'
    HYBRID = 'hybrid'
    MODE_CHOICES = [
        (ONSITE, 'Onsite'),
        (ONLINE, 'Online'),
        (HYBRID, 'Hybrid'),
    ]

    mode = models.CharField(max_length=10, choices=MODE_CHOICES) 
    venue = models.CharField(max_length=1000)

    def __str__(self):
        return '{} [{}]'.format(self.mode.capitalize(), self.venue)

class Event(models.Model):
    target_datetime = models.DateTimeField()
    activity = models.TextField(max_length=100)
    estimated_hours = models.FloatField()
    location = models.ForeignKey(Location, on_delete=models.CASCADE)
    course = models.CharField(max_length=1000)

    def __str__(self):
        return '{}, {}'.format(self.target_datetime, self.activity)
    
    def event_name(self):
        return '{}'.format(self.activity)
    
    def get_absolute_url(self):
        return reverse('widget_calendar:event-details', kwargs={'pk':self.pk})
from django.contrib import admin

from.models import Event, Location


class EventAdmin(admin.ModelAdmin):
    model = Event


class LocationAdmin(admin.ModelAdmin):
    model = Location


admin.site.register(Event, EventAdmin)
admin.site.register(Location, LocationAdmin)
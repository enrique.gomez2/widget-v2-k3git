from django.shortcuts import render
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView

from.models import WidgetUser


def index(request):
    return render(request, 'dashboard/dashboard.html', {'widgetusers': WidgetUser.objects.all()})


class WidgetUserDetailView(DetailView):
	model = WidgetUser

	def get(self, request, pk):
		return render(request, 'dashboard/widgetuser-details.html', {'widgetuser': self.model.objects.get(pk=pk)})
	

class WidgetUserCreateView(CreateView):
	model = WidgetUser
	fields = '__all__'
	template_name = 'dashboard/widgetuser-add.html'
	

class WidgetUserUpdateView(UpdateView):
	model = WidgetUser
	fields = '__all__'
	template_name = 'dashboard/widgetuser-edit.html'
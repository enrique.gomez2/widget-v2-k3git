from django.shortcuts import render
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView

from .models import Announcement

def index(request):
    return render(request, 'announcement/announcement.html', {'announcement': Announcement.objects.all()})

class AnnouncementDetailView(DetailView):
    model = Announcement

    def get(self, request, pk):
        return render(request, 'announcement/announcement-details.html', {'announcement': self.model.objects.get(pk=pk)})
    
class AnnouncementCreateView(CreateView):
    model = Announcement
    fields = '__all__'
    template_name = 'announcement/announcement-add.html'

class AnnouncementUpdateView(UpdateView):
    model = Announcement
    fields = '__all__'
    template_name = 'announcement/announcement-edit.html'


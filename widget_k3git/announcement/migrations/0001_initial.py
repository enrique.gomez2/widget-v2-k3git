# Generated by Django 4.1.7 on 2023-03-07 03:33

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Announcement',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=100, null=True)),
                ('body', models.TextField(null=True)),
                ('author', models.CharField(max_length=100, null=True)),
                ('pub_datetime', models.DateTimeField(null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Reaction',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(choices=[('Like', 'Like'), ('Heart', 'Heart'), ('Angry', 'Angry')], default='Like', max_length=10)),
                ('tally', models.IntegerField(null=True)),
                ('announcement', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='announcement.announcement')),
            ],
        ),
    ]

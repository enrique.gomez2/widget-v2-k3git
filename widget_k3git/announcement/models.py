from django.db import models
from django.urls import reverse

# Create your models here.
class Announcement(models.Model):
    title = models.CharField(max_length=100, null = True)
    body = models.TextField(null = True)
    author = models.CharField(max_length=100, null = True)
    pub_datetime = models.DateTimeField(null = True)

    def __str__(self):
        return '{} by {} published {}: {}'.format(self.title, self.author, self.pub_datetime, self.body)
    
    def get_absolute_url(self):
        return reverse('announcement:announcement-details', kwargs={'pk':self.pk})

class Reaction(models.Model):
    reaction_list = [("Like", "Like"), ("Heart", "Heart"), ("Angry", "Angry")]
    name = models.CharField(max_length=10, choices = reaction_list, default = "Like")
    tally = models.IntegerField(null = True)
    announcement = models.ForeignKey(Announcement, on_delete=models.CASCADE)

    def __str__(self):
        return '{}: {}'.format(self.name, self.tally)

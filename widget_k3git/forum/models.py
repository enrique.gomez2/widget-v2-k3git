from django.db import models
from django.urls import reverse


class ForumPost(models.Model):
    title = models.CharField(max_length=100)
    body = models.TextField(max_length=1000)
    author = models.CharField(max_length=100)
    pub_datetime = models.DateTimeField()

    def __str__(self):
        return '{} by {}'.format(self.title, self.author)

    def get_absolute_url(self):
        return reverse('forum:forumpost-details', kwargs={'pk': self.pk})


class Reply(models.Model):
    body = models.TextField(max_length=1000)
    author = models.CharField(max_length=100)
    pub_datetime = models.DateTimeField()
    forum_post = models.ForeignKey(ForumPost, on_delete=models.CASCADE)

    def __str__(self):
        return '{}\'s reply to {}'.format(self.author, self.forum_post)


# Create your models here.

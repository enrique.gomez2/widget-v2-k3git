from django.contrib import admin

from .models import ForumPost, Reply


class ForumPostAdmin(admin.ModelAdmin):
    model = ForumPost


class ReplyAdmin(admin.ModelAdmin):
    model = Reply
# Register your models here.


admin.site.register(ForumPost, ForumPostAdmin)
admin.site.register(Reply, ReplyAdmin)
from django.contrib import admin
from django.urls import path

from .views import index, ForumPostDetailView, ForumPostCreateView, ForumPostUpdateView

urlpatterns = [
    path('', index, name='index'),
    path('forumposts/<int:pk>/details', ForumPostDetailView.as_view(), name='forumpost-details'),
    path('forumposts/add', ForumPostCreateView.as_view(), name='forumpost-add'),
    path('forumposts/<int:pk>/edit', ForumPostUpdateView.as_view(), name='forumpost-edit'),
]

app_name = "forum"
from django.shortcuts import render
from django.http import HttpResponse
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView

from .models import ForumPost, Reply


def index(request):
    return render(request, 'forum/forum.html', {'forumposts': ForumPost.objects.all()})


class ForumPostDetailView(DetailView):
    model = ForumPost

    def get(self, request, pk):
        return render(request, 'forum/forumpost-details.html', {'forum_post': self.model.objects.get(pk=pk)})


class ForumPostCreateView(CreateView):
    model = ForumPost
    fields = '__all__'
    template_name = 'forum/forumpost-add.html'


class ForumPostUpdateView(UpdateView):
    model = ForumPost
    fields = '__all__'
    template_name = 'forum/forumpost-edit.html'

# Create your views here.

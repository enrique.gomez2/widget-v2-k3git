from django.contrib import admin
from django.urls import include, path

urlpatterns = [
    path('announcements/', include('announcement.urls', namespace="announcement")),
    path('calendar/', include('widget_calendar.urls', namespace="widget_calendar")),
    path('assignments/', include('assignments.urls', namespace="assignments")),
    path('dashboard/', include('dashboard.urls', namespace="dashboard")),
    path('forum/', include('forum.urls', namespace="forum")),
    path('admin/', admin.site.urls),
]
